<?php

/**
 * Product price and stock managment
 */

namespace KP_Moneo;

defined('ABSPATH') || exit;

class Products extends Moneo
{
	public function __construct()
	{
		//add actions and filters
		add_action('init', [$this, 'kp_moneo_webhook']);
		add_filter('check_if_product_in_moneo', [$this, 'check_if_product_in_moneo']);
	}

	//Check if product is in moneo
	public function check_if_product_in_moneo($sku)
	{
		$data = $this->get_moneo_data_by_sku($sku);

		if (isset($data[0]->code) && !empty($data[0]->code)) {
			return true;
		}

		return false;
	}

	//Get product by sku
	public function get_product_by_sku($sku)
	{
		global $wpdb;

		$product_id = $wpdb->get_var($wpdb->prepare("SELECT post_id FROM $wpdb->postmeta WHERE meta_key='_sku' AND meta_value='%s' LIMIT 1", $sku));

		if ($product_id) {
			return new \WC_Product($product_id);
		}

		return null;
	}

	//Moneo webhook functionality, for updating and inserting new products
	public function kp_moneo_webhook()
	{
		if (!isset($_GET['kp-api'])) return;
		if ($_GET['kp-api'] != 'wc_moneo') return;
		if (!isset($_POST)) return;

		$data = json_decode(file_get_contents('php://input'), true);
		if (!isset($data['code']) && !isset($data['sub_tables']['items'])) return;

		parent::log('info', 'Saņemts izejošais pieprasījums');
		parent::log('info', json_encode($data), true);

		//Single code sent
		if (isset($data['code'])) {
			$product = $this->get_product_by_sku($data['code']);

			if ($product) {
				$this->update_product_data_from_moneo($product, $data['code']);
			} else {
				$this->insert_product_data_from_moneo($data['code']);
			}
		}

		//Multiple products sent
		if (isset($data['sub_tables']['items'])) {
			foreach ($data['sub_tables']['items'] as $item) {
				if (isset($item['code'])) {
					$code = $item['code'];
					$product = $this->get_product_by_sku($code);
					if ($product) {
						$this->update_product_data_from_moneo($product, $code);
					} else {
						$this->insert_product_data_from_moneo($code);
					}
				}
			}
		}

		parent::log('info', 'Izejošais pieprasījums pabeigts');

		echo 1;
		exit;
	}

	//Update existing product
	public function update_product_data_from_moneo($product, $sku)
	{
		if (!$product || !$sku) {
			return;
		}

		$moneo_data = $this->get_moneo_data_by_sku($sku);

		//Empty moneo data means, that product was deleted, so we trash it
		if (is_array($moneo_data) && empty($moneo_data)) {
			wp_trash_post($product->get_id());
		}

		if (isset($moneo_data[0])) $moneo_data = $moneo_data[0];

		if (!isset($moneo_data->name)) return;

		$this->update_product_properties($moneo_data, $product);
		$this->update_product_stock($product, $sku);
	}

	//Insert new product
	public function insert_product_data_from_moneo($sku)
	{
		if (!$sku) {
			return;
		}

		$moneo_data = $this->get_moneo_data_by_sku($sku);

		if (isset($moneo_data[0])) {
			$moneo_data = $moneo_data[0];
		}

		if (!isset($moneo_data->name)) {
			return;
		}

		//Insert new post
		$post_id = wp_insert_post(array(
			'post_title' => $moneo_data->name,
			'post_type' => 'product',
			'post_status' => 'draft',
		));

		if (!$post_id) {
			return;
		}

		$product = wc_get_product($post_id);

		$this->update_product_properties($moneo_data, $product);
		$this->update_product_stock($product, $sku);
	}

	//Update/Add product properties
	public function update_product_properties($moneo_data, $product)
	{
		if (isset($moneo_data->code)) {
			if (isset($moneo_data->cust_cena_ar_pvn)) {
				parent::log('info', 'Cena: ' . $moneo_data->cust_cena_ar_pvn . ' SKU: ' . $moneo_data->code);
			} else {
				parent::log('warning', 'Nav cena ar pvn: ' . $moneo_data->code);
			}
		} else {
			parent::log('warning', 'Nav sku: ' . $product->get_id());
		}

		//Set title
		if (isset($moneo_data->name)) {
			$title = preg_replace('/(?>, *| *)FSC.*/', '', $moneo_data->name);
			$product->set_name($title);
		}

		//Set SKU
		if (isset($moneo_data->code)) {
			$product->set_sku($moneo_data->code);
		}

		//Set price
		if (isset($moneo_data->cust_cena_ar_pvn)) {
			$product->set_regular_price($moneo_data->cust_cena_ar_pvn);
		}

		//Set weight
		if (isset($moneo_data->netweight) && $moneo_data->netweight > 0) {
			$product->set_weight($moneo_data->netweight);
		}

		//Set width
		if (isset($moneo_data->width) && $moneo_data->width > 0) {
			$product->set_width($moneo_data->width);
		}

		//Set length
		if (isset($moneo_data->length) && $moneo_data->length > 0) {
			$product->set_length($moneo_data->length);
		}

		//Set height
		if (isset($moneo_data->cust_biezums) && $moneo_data->cust_biezums > 0) {
			$product->set_height($moneo_data->cust_biezums);
		}

		//Update unit
		if (isset($moneo_data->unittext)) {
			update_post_meta($product->get_id(), 'price_unit', $moneo_data->unittext);
		}

		$product->save();
	}

	public function update_product_stock($product, $sku)
	{
		if (!$sku) {
			return;
		}

		$data = [
			"params" => [
				[$sku],
				[
					"ItemGroup" => "",
					"ItemClass" => "",
					"BalanceLocation" => "",
					"Location" => "",
					"Supplier" => "",
					"IncludeInStock" => 3,
					"IncludeNotAcceptedInvoices" => 0,
					"IncludeOpenPOSInvoices" => 0,
					"IncludeNotAcceptedStockMoves" => 0,
					"IncludeNotAcceptedProd" => 0,
					"IncludeNotAcceptedWorkTables" => 0,
					"IncludeNotAcceptedWriteoffs" => 0,
					"IncludeNotAcceptedSord" => "1",
					"IgnoreMinLevels" => 0
				]
			]
		];

		$response = parent::request($data, 'stock');
		$quantity = isset($response->result[0]->availqty) ? $response->result[0]->availqty : 0;

		$product->set_manage_stock(true);

		if ($quantity > 0) {
			$product->set_stock_quantity($quantity);
			$product->set_stock_status('instock');
		} else {
			$product->set_stock_quantity(0);
			$product->set_stock_status('outofstock');
		}

		$product->save();
	}


	//Gets all the product skus
	public function get_all_moneo_skus()
	{
		$data = [
			"fieldlist" => ["code"]
		];
		return parent::request($data, 'items');
	}

	//Get product data from moneo by code/sku
	public function get_moneo_data_by_sku($sku)
	{
		if (!$sku) {
			return;
		}

		$data = [
			"filter" => [
				"code" => $sku,
			],
		];

		$response = parent::request($data, 'items');

		if (isset($response->result->records)) {
			return $response->result->records;
		}

		return null;
	}

	//Get product stock by sku
	public function get_moneo_stock_by_sku($sku)
	{
		if (!$sku) {
			return;
		}

		$data = [
			"params" => [
				[$sku],
				[
					"ItemGroup" => "",
					"ItemClass" => "",
					"BalanceLocation" => "",
					"Location" => "",
					"Supplier" => "",
					"IncludeInStock" => 3,
					"IncludeNotAcceptedInvoices" =>  0,
					"IncludeOpenPOSInvoices" =>  0,
					"IncludeNotAcceptedStockMoves" =>  0,
					"IncludeNotAcceptedProd" =>  0,
					"IncludeNotAcceptedWorkTables" =>  0,
					"IncludeNotAcceptedWriteoffs" =>  0,
					"IncludeNotAcceptedSord" =>  "1",
					"IgnoreMinLevels" =>  0
				]
			],
		];

		return parent::request($data, 'stock');
	}
}
