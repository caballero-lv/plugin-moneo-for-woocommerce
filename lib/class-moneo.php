<?php

/**
 * Moneo wrapper
 */

namespace KP_Moneo;

defined('ABSPATH') || exit;

class Moneo
{
	public function get_options()
	{
		return get_option('moneo_plugin_options');
	}

	public function request($data = [], $type = '')
	{
		$options = $this->get_options();

		$url = 'https://' . $options['api_url'] . '.moneo.lv:' . $options['api_port'] . '/api/v2/';

		// Diferent request
		switch ($type) {
			case 'create_order':
				$url .= 'sorders.orders/create/';
				break;
			case 'get_contact':
				$url .= 'contacts.contacts/';
				break;
			case 'create_contact':
				$url .= 'contacts.contacts/create/';
				break;
			case 'items':
				$url .= 'items.items/';
				break;
			case 'stock':
				$url .= 'method/stock.getItemDeficiencyInfo/';
				break;
		}

		$data['request'] = [
			'compuid' => $options['api_compuid']
		];

		$data = json_encode($data);

		// Curl
		$ch = curl_init($url);
		// Return result of POST request
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		// Get information about last transfer
		curl_setopt($ch, CURLINFO_HEADER_OUT, true);
		// Use POST request
		curl_setopt($ch, CURLOPT_POST, true);
		// Set payload for POST request
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		// Set HTTP Header for POST request 
		curl_setopt(
			$ch,
			CURLOPT_HTTPHEADER,
			array(
				'Content-Type: application/json',
				'authorization: ' . $options['api_password'],
				'Content-Length: ' . strlen($data)
			)
		);
		// Execute a cURL session
		$result = json_decode(curl_exec($ch));
		// Close cURL session
		curl_close($ch);

		return $result;
	}

	public static function log($type, $message) {
		$upload_dir = wp_upload_dir();
		$log_file = $upload_dir['basedir'] . '/moneo.log';

		$fh = fopen($log_file, 'a');
		fwrite($fh, date('Y-m-d H:i:s') . ' [' . $type . '] ' . $message . "\n");
		fclose($fh);
	}
}
