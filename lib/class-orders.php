<?php

/**
 * Orders class
 */

namespace KP_Moneo;

defined('ABSPATH') || exit;

class Orders extends Moneo
{
	public function __construct()
	{
		//add actions and filters
		add_action('woocommerce_thankyou', [$this, 'new_moneo_order']);
		// add_action('send_order_to_moneo', [$this, 'new_moneo_order']);
	}

	/**
	 * Moneo get client and return it's id
	 */
	public function get_client($email)
	{
		$data = array(
			'filter' => array(
				'email' => $email,
			),
		);

		$response = parent::request($data, 'get_contact');

		if (!empty($response->result->records)) {
			$record = reset($response->result->records);
			return $record->code;
		}

		return null;
	}

	/**
	 * Moneo create client and return it's id
	 */
	public function create_client($name, $email, $phone, $iban, $vat)
	{
		$data = array(
			'data' => array(
				'contacts.contacts' => array(
					'fieldlist' => array("name", "email", "phone", "iban", "customerflag", "vatno"),
					'data' => array(
						array($name, $email, $phone, $iban, '1', $vat)
					)
				)
			),
		);

		$result = parent::request($data, 'create_contact');

		if (isset($result->result[0][1][4][0])) {
			return $result->result[0][1][4][0];
		}

		return null;
	}

	/**
	 * On thank you create new order
	 */
	public function new_moneo_order($order_id)
	{
		if (!$order_id) {
			return;
		}

		$moneo_id = get_post_meta($order_id, '_moneo_id', true);
		if ($moneo_id) {
			return;
		}

		$order = new \WC_Order($order_id);

		if (!is_a($order, 'WC_Order')) {
			return;
		}

		$data = $this->get_order_data($order);

		$response = parent::request($data, 'create_order');

		$success = $response->result[0][0];

		if ($success) {
			$moneo_id = $response->result[0][1][4][0];
			update_post_meta($order_id, '_moneo_id', $moneo_id);
		}
	}

	/**
	 * Get order data
	 */
	private function get_order_data($order)
	{
		$order_meta_data = get_post_meta($order->get_id());

		//Contact info
		$person_type = isset($order_meta_data['_billing_person_type'][0]) ? $order_meta_data['_billing_person_type'][0] : 'private';
		if ('private' == $person_type) {
			$full_name = isset($order_meta_data['_billing_full_name'][0]) ? $order_meta_data['_billing_full_name'][0] : '';
		} else {
			$full_name = isset($order_meta_data['_billing_company'][0]) ? $order_meta_data['_billing_company'][0] : '';
		}
		$phone = isset($order_meta_data['_billing_phone'][0]) ? $order_meta_data['_billing_phone'][0] : '';
		$email = isset($order_meta_data['_billing_email'][0]) ? $order_meta_data['_billing_email'][0] : '';
		$account = isset($order_meta_data['_billing_account_number'][0]) ? $order_meta_data['_billing_account_number'][0] : 'N/A';
		$note = isset($order_meta_data['_billing_note'][0]) ? $order_meta_data['_billing_note'][0] : '';
		$vat = isset($order_meta_data['_billing_pvn'][0]) ? $order_meta_data['_billing_pvn'][0] : 'N/A';

		//Shipping info
		$city = isset($order_meta_data['_shipping_city'][0]) ? $order_meta_data['_shipping_city'][0] : '';
		$address = isset($order_meta_data['_shipping_address_1'][0]) ? $order_meta_data['_shipping_address_1'][0] : '';
		$postcode = isset($order_meta_data['_shipping_postcode'][0]) ? $order_meta_data['_shipping_postcode'][0] : '';
		$time = isset($order_meta_data['_shipping_delivery_time'][0]) ? $order_meta_data['_shipping_delivery_time'][0] : '';
		$country = isset($order_meta_data['_shipping_country'][0]) ? $order_meta_data['_shipping_country'][0] : '';

		$client = $this->get_client($email);
		if (!$client) {
			$client = $this->create_client($full_name, $email, $phone, $account, $vat);
		}

		$delivery_address = $address ? $address . ' ' : '';
		$delivery_address .= $city ? $city . ' ' : '';
		$delivery_address .= $country ? $country . ' ' : '';
		$delivery_address .= $postcode ? $postcode . ' ' : '';

		$note .= $time ? ', Vēlamais piegādes laiks: ' . $time : '';

		$data = array(
			'data' => array(
				'sorders.orders' => array(
					'fieldlist' => array("custcode", "custname", "address1", "comment"),
					'data' => array(
						array($client, $full_name, $delivery_address, $note)
					)
				),
				'sorders.orders_items_rows' => array(
					'fieldlist' => array("itemcode", "quant", "price"),
					'data' => array()
				)
			),
		);

		foreach ($order->get_items() as $item) {
			$product = wc_get_product($item->get_product_id());
			if (apply_filters('check_if_product_in_moneo', $product->get_sku())) {
				$price_vat = $product->get_price();
				$price = round($price_vat / 1.21, 2);

				$data['data']['sorders.orders_items_rows']['data'][] = array(
					$product->get_sku(),
					$item->get_quantity(),
					$price
				);
			}
		}

		return $data;
	}
}
