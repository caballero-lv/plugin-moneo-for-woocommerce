<?php
namespace KP_Moneo;

defined('ABSPATH') || exit;

class Settings
{
	public function __construct()
	{
		add_action('admin_menu', [$this, 'moneo_add_settings_page']);
		add_action('admin_init', [$this, 'moneo_register_settings']);
	}

	public function moneo_add_settings_page()
	{
		add_options_page('Moneo', 'Moneo Iestatījumi', 'manage_options', 'moneo-settings', [$this, 'moneo_render_plugin_settings_page']);
	}

	public function moneo_render_plugin_settings_page()
	{
		echo '<h2>Moneo Iestatījumi</h2>';
		echo '<form action="options.php" method="post">';
		settings_fields('moneo_plugin_options');
		do_settings_sections('moneo_plugin');
		echo '<input name="submit" class="button button-primary" type="submit" value="Saglabāt" />';
		echo '</form>';
	}

	public function moneo_register_settings()
	{
		add_settings_section('api_settings', 'API Iestatījumi', [$this, 'moneo_section_text'], 'moneo_plugin');
		register_setting('moneo_plugin_options', 'moneo_plugin_options');

		add_settings_field('moneo_settings_api_username', 'Lietotājvārds', [$this, 'moneo_settings_api_username'], 'moneo_plugin', 'api_settings');
		add_settings_field('moneo_settings_api_password', 'API Atslēga', [$this, 'moneo_settings_api_password'], 'moneo_plugin', 'api_settings');
		add_settings_field('moneo_settings_api_compuid', 'Company ID', [$this, 'moneo_settings_api_compuid'], 'moneo_plugin', 'api_settings');
		add_settings_field('moneo_settings_api_url', 'API URL Prefiks', [$this, 'moneo_settings_api_url'], 'moneo_plugin', 'api_settings');
		add_settings_field('moneo_settings_api_port', 'API Ports', [$this, 'moneo_settings_api_port'], 'moneo_plugin', 'api_settings');
	}

	public function moneo_section_text()
	{
		echo '<p>Šeit Jūs varar mainīt moneo API piekļuves informāciju</p>';
	}

	public function moneo_settings_api_username()
	{
		$options = get_option('moneo_plugin_options');
		echo "<input id='moneo_settings_api_username' name='moneo_plugin_options[api_username]' type='text' value='" . esc_attr($options['api_username']) . "'/>";
	}

	public function moneo_settings_api_password()
	{
		$options = get_option('moneo_plugin_options');
		echo "<input id='moneo_settings_api_password' name='moneo_plugin_options[api_password]' type='text' value='" . esc_attr($options['api_password']) . "'/>";
	}

	public function moneo_settings_api_compuid()
	{
		$options = get_option('moneo_plugin_options');
		echo "<input id='moneo_settings_api_compuid' name='moneo_plugin_options[api_compuid]' type='text' value='" . esc_attr($options['api_compuid']) . "'/>";
	}

	public function moneo_settings_api_url()
	{
		$options = get_option('moneo_plugin_options');
		echo "<input id='moneo_options_api_url' name='moneo_plugin_options[api_url]' type='text' value='" . esc_attr($options['api_url']) . "'/>";
	}

	public function moneo_settings_api_port()
	{
		$options = get_option('moneo_plugin_options');
		echo "<input id='moneo_options_api_port' name='moneo_plugin_options[api_port]' type='text' value='" . esc_attr($options['api_port']) . "'/>";
	}
}
