<?php
/**
 * Plugin Name: Moneo for Woocommerce
 * Description: Moneo woocommerce integration made for katesplates.lv
 * Version: 1.2.15
 * Author: Caballero
 */

defined('ABSPATH') || exit;

//Include required stuff for functionality
add_action('plugins_loaded', function () {
	//Check if woocommerce plugin is active
	if (!class_exists('WooCommerce')) return;

	//include class that manages moneo requests
	require_once __DIR__ . "/lib/class-moneo.php";
	//include class that manages moneo settings in wp-admin
	require_once __DIR__ . "/lib/class-moneo-settings.php";
	//include class that manages products kates plates to moneo
	require_once __DIR__ . "/lib/class-products.php";
	//include class that manages products moneo from kates plates
	require_once __DIR__ . "/lib/class-orders.php";

	new KP_Moneo\Settings();
	new KP_Moneo\Products();
	new KP_Moneo\Orders();
});
